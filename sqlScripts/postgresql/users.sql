CREATE TABLE IF NOT EXISTS user_sexs(
	id 		SERIAL NOT NULL,
	name 	VARCHAR(50) NOT NULL,
	CONSTRAINT pk_user_sexs PRIMARY KEY(id),
	CONSTRAINT uk_user_sexs_name UNIQUE(name)
);

CREATE TABLE IF NOT EXISTS user_groups(
	id 		SERIAL NOT NULL,
	name 	VARCHAR(50) NOT NULL,
	CONSTRAINT pk_user_groups PRIMARY KEY(id),
	CONSTRAINT uk_user_groups_name UNIQUE(name)
);

CREATE TABLE IF NOT EXISTS users(
	id 						SERIAL NOT NULL,
	login 					VARCHAR(50) NOT NULL,
	password 				VARCHAR(50) NOT NULL,
	first_name 				VARCHAR(50) NOT NULL,
	name 					VARCHAR(50) NOT NULL NOT NULL,
	email 					VARCHAR(255) NOT NULL,
	birth_date 				DATE NOT NULL,
	registration_date 		TIMESTAMP,
	connection_last_date 	TIMESTAMP,
	validation_date 		TIMESTAMP,
	token 					VARCHAR(255),
	fk_user_sex				INTEGER NOT NULL,
	fk_user_group			INTEGER NOT NULL,
	CONSTRAINT pk_users PRIMARY KEY(id),
	CONSTRAINT uk_users_login UNIQUE(login),
	CONSTRAINT uk_users_email UNIQUE(email),
	CONSTRAINT uk_users_token UNIQUE(token),
	CONSTRAINT fk_user_sexs_users FOREIGN KEY(fk_user_sex) REFERENCES user_sexs(id),
	CONSTRAINT fk_group_users_users FOREIGN KEY(fk_user_group) REFERENCES user_groups(id)
);

CREATE INDEX ind_users_login ON users(login);

CREATE FUNCTION users_insert() RETURNS trigger as $$
BEGIN
	NEW.registration_date = CURRENT_TIMESTAMP;
	NEW.connection_last_date = NEW.registration_date;
	NEW.validation_date = NULL;
	return NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER users_insert BEFORE INSERT ON users
	FOR EACH ROW EXECUTE PROCEDURE users_insert();

CREATE FUNCTION users_update() RETURNS trigger as $$
BEGIN
	NEW.registration_date = OLD.registration_date;
	NEW.connection_last_date = CURRENT_TIMESTAMP;
	return NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER users_update BEFORE UPDATE ON users
	FOR EACH ROW EXECUTE PROCEDURE users_update();