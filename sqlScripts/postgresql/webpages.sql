CREATE TABLE IF NOT EXISTS webpages(
	id					SERIAL NOT NULL,
	alias				VARCHAR(50),
	date_create			TIMESTAMP NOT NULL,
	date_update			TIMESTAMP NOT NULL,
	meta_description	VARCHAR(255),
	meta_keywords		VARCHAR(255),
	title				VARCHAR(50) NOT NULL,
	CONSTRAINT pk_webpage PRIMARY KEY(id),
	CONSTRAINT ck_webpage_dates CHECK(date_create <= date_update),
	CONSTRAINT uk_webpage_alias UNIQUE(alias)
);

CREATE INDEX ind_webpages_alias ON webpages(alias);

CREATE FUNCTION webpages_insert_alias() RETURNS trigger as $$
DECLARE
	n_aliases INTEGER;
BEGIN
	NEW.alias := NEW.title;
	select count(*) into n_aliases from webpages where alias LIKE NEW.alias || '%';
	IF n_aliases <> 0 THEN
		NEW.alias := NEW.alias || '_' || n_aliases;
	END IF;
	return NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER webpages_insert_alias BEFORE INSERT ON webpages
	FOR EACH ROW EXECUTE PROCEDURE webpages_insert_alias();

CREATE FUNCTION webpages_update_alias() RETURNS trigger as $$
BEGIN
	NEW.alias := OLD.alias;
	NEW.date_create := OLD.date_create;
	return NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER webpages_update_alias BEFORE UPDATE ON webpages
	FOR EACH ROW EXECUTE PROCEDURE webpages_update_alias();