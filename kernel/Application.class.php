<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	/**
	 * The class who manage the web application<br/>
	 * This class is a singleton
	 */
	class Application{
		/**
		 * Instance of Application<br/>
		 * @var Application
		 */
		private static $_instance;
		/**
		 * Attribute connect to database<br/>
		 * Is defined by a url component 
		 * @var Database
		 */
		private $_database;
		/**
		 * Language of the application
		 */
		private $_lang;
		
		public static $unauthorized	= "/unauthorized";
		public static $forbidden	= "/forbidden";
		public static $notfound		= "/notfound";
		private function __construct(){
			$this->getLangApplication();
			$this->loadKernelComponents();
			$this->loadComponents();
		}
		private function __clone(){}
		/**
		 * Load core components predefined by the function defined_core_components()
		 */
		public function loadKernelComponents(){
			$dir = __DIR__.'/components';
			$underDir = array_diff(scandir($dir), array('.','..'));
			foreach ($underDir as $component){
				$file = $dir.'/'.$component.'/main.php';
				if(is_file($file))
					require_once $dir.'/'.$component.'/main.php';
				else
					trigger_error('Component '.$component.' of kernel must have a file main.php', E_USER_ERROR);
			}
		}
		/**
		 * Load all components
		 */
		public function loadComponents(){
			$dir = __DIR__.'/../components';
			$underDir = array_diff(scandir($dir), array('.','..'));
			foreach ($underDir as $component){
				require_once $dir.'/'.$component.'/Component'.ucfirst($component).'.class.php';
				$c = 'Component'.ucfirst($component);
				if (!is_a(new $c(NULL), 'Component')){
					trigger_error('class Component'.ucfirst($component).' must extend class Component', E_USER_ERROR);
				}
			}
		}
		/**
		 * Give the hand to the domain to load good activities
		 */
		public function getDomain($domainName){
			require_once __DIR__.'/../domains/'.$domainName."/main.php";
		}
		/**
		 * Get an action to specific component
		 * @param name of component $component
		 * @param name of action $action
		 * @return a component extends by Component class
		 */
		public function getNameClassComponent($component){
			return 'Component'.ucfirst($component);
		}
		/**
		 * Get an _instance of Application
		 */
		public static function getInstance(){
			if(self::$_instance != null)
				return self::$_instance;
			self::$_instance = new Application();
			return self::$_instance; 
		}
		/**
		 * Defined the language of user to application
		 */
		private function getLangApplication(){
			if(!isset($_COOKIE['lang'])){
				$gettedLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
				setcookie('lang', $gettedLang, time()+2592000); // 30 days
				$this->_lang = $gettedLang;
			}else{
				$this->_lang = $_COOKIE['lang'];
			}
		}
		/**
		 * Get the language of user
		 */
		public function getLang(){
			return $this->_lang;
		}
		/**
		 * Set the language of user
		 * @param lang $lang
		 */
		public function setLang($lang){
			setcookie('lang', $lang, time()+2592000); // 30 days
			$this->getLangApplication();
		}
		/**
		 * Get the database
		 * @return Database
		 */
		public function getDatabase(){
			return $this->_database;
		}
		/**
		 * Set the database
		 * @param Database $database
		 */
		public function setDatabase($database){
			$this->_database = $database;
		}
	}
?>