<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	/**
	 * Type of action of the component action
	 * @author Kevin
	 */
	abstract class ActionTypeComponent{
		/**
		 * Display a display
		 * @var DISPLAY
		 */
		const DISPLAY = 0;
		/**
		 * Display a form
		 * @var FORM
		 */
		const FORM = 1;
		/**
		 * Do an action in backfront
		 * @var PROCESS
		 */
		const PROCESS = 2;
	}
?>