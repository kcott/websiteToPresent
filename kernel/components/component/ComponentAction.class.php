<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	/**
	 * Action of a component
	 * @author Kevin
	 */
	class ComponentAction{
		private $_name;
		private $_actionType;
		
		/**
		 * Construct an action of a component
		 * @param string $name
		 * @param ActionTypeComponent $actionType
		 */
		public function __construct($name, $actionType){
			$this->_name = $name;
			$this->_actionType = $actionType;
		}
		/**
		 * Get the name
		 * @return string
		 */
		public function getName(){
			return $this->_name;
		}
		/**
		 * Get the action type
		 * @return ActionTypeComponent
		 */
		public function getActionType(){
			return $this->_actionType;
		}
	}
?>