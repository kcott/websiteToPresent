<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	require_once __DIR__.'/ActionTypeComponent.class.php';
	require_once __DIR__.'/ComponentAction.class.php';
	require_once __DIR__.'/Component.class.php';
?>