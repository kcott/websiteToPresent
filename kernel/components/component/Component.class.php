<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	abstract class Component{
		private $_params;
		private $_actions;
		static private $_links;
		/**
		 * Construct a component with $params
		 * @param string array $params
		 */
		public function __construct($params){
			$this->_params = $params;
			$this->_actions = array();
			$this->_links = array();
		}
		/**
		 * Get params of the component
		 * @return string array
		 */
		public function getParams(){
			return $this->_params;
		}
		/**
		 * Set params of the application
		 * @param string array $params
		 */
		public function setParams($params){
			$this->_params = $params;
		}
		/**
		 * Do an action of the component
		 * @param string $action
		 */
		abstract public function doAction($action);
		/**
		 * Get the possible actions of the component
		 * @return string array
		 */
		public function getActions(){
			return $this->_actions();
		}
		/**
		 * Add an action $action with a type $type
		 * @param string $key
		 * @param ComponentAction $action
		 */
		protected function addAction($key, $action){
			$this->_actions[$key] = $action;
		}
		static protected function addLink($key, $value){
			self::$_links[$key] = $value;
		}
		static public function getLink($key){
			if(array_key_exists($key, self::$_links))
				return self::$_links[$key];
			else
				return NULL;
		}
	}
?>