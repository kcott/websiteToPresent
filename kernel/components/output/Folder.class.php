<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class Folder{
		static function deleteDirectory($dir){
			$files = array_diff(scandir($dir), array('.','..'));
			foreach ($files as $file){
				if(is_dir("$dir/$file"))
					deleteDirectory("$dir/$file");
				else
					unlink("$dir/$file");
			}
			return rmdir($dir);
		}
	}
?>