<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class EntryFormRadio extends EntryFormInput{
		/**
		 * Values to display
		 * @var array of string
		 */
		private $values;
		/**
		 * Create a radio group to a form
		 * @param string $name
		 * @param string $text
		 * @param string array $constraints to all radio
		 */
		public function __construct(
				$name,
				$text,
				$constraints){
			parent::__construct('radio', $name, '', $text, $constraints);
			$this->values = array();
		}
		/**
		 * Add a value to the radio
		 * @param string $option
		 * @param string description
		 * @param string array $constraints to this radio
		 */
		public function addValue($value, $description, $constraints){
			$this->values[$value] = array($description, $constraints);
		}
		/**
		 * Display the input with html tags
		 */
		public function generate(){
			echo '<tr>';
			echo '<td>'.$this->getText().'</td>';
			echo '<td>';
			foreach ($this->values as $key => $value){
				$id = 'form_'.$this->getName().'_'.$key;
				echo '<input name="'.$this->getName().'" ';
				echo 'type="'.$this->getType().'" ';
				echo 'value="'.$key.'" ';
				echo 'id="'.$id.'" ';
				foreach ($this->getConstraints() as $c){
					echo $c.' ';
				}
				foreach ($value[1] as $c){
					echo $c.' ';
				}
				echo '/>';
				echo '<label for="'.$id.'">'.$value[0].'</label>';
			}
			echo '</td>';
			echo '</tr>';
		}
	}
?>