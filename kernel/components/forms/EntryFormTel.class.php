<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class EntryFormTel extends EntryFormInput{
		/**
		 * Create an entry to a form
		 * @param string $name
		 * @param string $value
		 * @param string $text
		 * @param array with key the constraint and the element the value $constraints
		 */
		public function __construct(
				$name,
				$value,
				$text,
				$constraints){
			parent::__construct('tel', $name, $value, $text, $constraints);
		}
		/**
		 * Check whether a number is a phone number (french phone version)<br/>
		 * Format of ten digit
		 * @param string of digits $number
		 */
		public static function isPhone($number){
			return preg_match('/^[0-9]{10}$/', $number);
		}
		/**
		 * Check whether the entry of the form is valid
		 * @return boolean
		 */
		public function validate(){
			if(!parent::validate())
				return false;
			if(isset($_POST[$this->getName()]) || !EntryFormTel::isPhone($_POST[$this->name]))
				return false;
			return true;
		}
	}
?>