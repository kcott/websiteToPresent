<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class EntryFormDate extends EntryFormInput{
		/**
		 * Create an entry to a form
		 * @param string $name
		 * @param string $value
		 * @param string $text
		 * @param array with key the constraint and the element the value $constraints
		 */
		public function __construct(
				$name,
				$value,
				$text,
				$constraints){
			parent::__construct('date', $name, $value, $text, $constraints);
		}
		/**
		 * Check whether a date have a good format
		 * @param string $date
		 */
		public static function isDate($date){
			$year	= substr($date, 0, 4);
			$month	= substr($date, 5, 2);
			$day	= substr($date, 8, 2);
			
			return checkdate($month, $day, $year);
		}
		/**
		 * Check whether the entry of the form is valid
		 * @return boolean
		 */
		public function validate(){
			if(!parent::validate())
				return false;
			if(isset($_POST[$this->getName()]))
				if(!EntryFormDate::isDate($_POST[$this->getName()]))
					return false;
			return true;
		}
	}
?>