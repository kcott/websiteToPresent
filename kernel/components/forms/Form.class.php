<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	/**
	 * Define a class to verify content of a form
	 * @author Kevin
	 */
	class Form extends Webform{
		/**
		 * Entries of the form
		 * @var Webform array
		 */
		private $_webforms;
		/**
		 * Legend of the fieldset of the form
		 * @var string
		 */
		private $_legend;
		/**
		 * Create a form
		 * @var string $legend
		 */
		public function __construct($legend){
			$this->_legend = $legend;
		}
		/**
		 * Return the legend of the form
		 * @return string
		 */
		public function getLegend(){
			return $this->_legend;
		}
		/**
		 * Set the legend of the form
		 * @param string $legend
		 */
		public function setLegend($legend){
			$this->_legend = $legend;
		}
		/**
		 * Add a webform in the form
		 * @param Webform $webform
		 */
		public function add($webform){
			$this->_webforms[] = $webform;
		}
		/**
		 * Generate a html5 form
		 */
		public function generate(){
			// Display all entries of form
			echo "<fieldset><legend><b>$this->_legend</b></legend><table>";
			foreach ($this->_webforms as $webform){
				$webform->generate();
			}
			echo '</table></fieldset>';
		}
		/**
		 * Check if every entries of the form are valid
		 * @return boolean
		 */
		public function validate(){
			if(!isset($_POST['submit_x']))
				return false;
			foreach ($this->_webforms as $webform){
				if($webform->validate() == false)
					return false;
			}
			return true;
		}
		/**
		 * Check if form is canceled
		 * @return boolean
		 */
		public function isCanceled(){
			if(isset($_POST['cancel_x']))
				return true;
			return false;
		}
	}
?>