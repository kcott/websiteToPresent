<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class EntryFormTextarea extends EntryForm{
		/**
		 * Create a textaera to a form
		 * @param string $name
		 * @param string $value
		 * @param string $text
		 * @param string array $constraints
		 */
		public function __construct(
				$name,
				$value,
				$text,
				$constraints){
			parent::__construct($name, $value, $text, $constraints);
		}
		/**
		 * Display the textarea with html tags
		 */
		public function generate(){
			$id = 'form_'.$this->getName().'_'.$this->getValue();
			echo '<tr>';
			echo '<td><label for="'.$id.'">'.$this->getText().'</label></td>';
			echo '<td>';
			echo '<textarea name="'.$this->getName().'" id="'.$id.'">';
			echo $this->getValue();
			echo '</textarea>';
			echo '</td>';
			echo '</tr>';
		}
		/**
		 * Check whether the entry of the form is valid
		 * @return boolean
		 */
		public function validate(){
			if(array_key_exists("required", $this->getConstraints())){
				if(!isset($_POST[$this->getName()]) || !EntryForm::isIsset($_POST[$this->getName()]))
					return false;
			}
			return true;
		}
	}
?>