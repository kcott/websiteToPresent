<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class EntryFormPassword extends EntryFormInput{
		/**
		 * Create a password entry to a form
		 * @param string $name
		 * @param string $value
		 * @param string $text
		 * @param array with key the constraint and the element the value $constraints
		 */
		public function __construct(
				$name,
				$value,
				$text,
				$constraints){
			parent::__construct('password', $name, $value, $text, $constraints);
		}
	}
?>