<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	require_once __DIR__.'/Webform.class.php';
	require_once __DIR__.'/EntryForm.class.php';
	require_once __DIR__.'/EntryFormInput.class.php';
	require_once __DIR__.'/EntryFormValidation.class.php';
	require_once __DIR__.'/EntryFormText.class.php';
	require_once __DIR__.'/EntryFormTextarea.class.php';
	require_once __DIR__.'/EntryFormTel.class.php';
	require_once __DIR__.'/EntryFormDate.class.php';
	require_once __DIR__.'/EntryFormPassword.class.php';
	require_once __DIR__.'/EntryFormEmail.class.php';
	require_once __DIR__.'/EntryFormSelect.class.php';
	require_once __DIR__.'/EntryFormRadio.class.php';
	require_once __DIR__.'/EntryFormCheckbox.class.php';
	require_once __DIR__.'/Form.class.php';
?>