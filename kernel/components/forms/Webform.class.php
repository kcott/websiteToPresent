<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	abstract class Webform{
		/**
		 * Display the entry with html tags
		 */
		abstract public function generate();
		/**
		 * Check whether the entry of the form is valid
		 * @return boolean
		 */
		abstract public function validate();
	}
?>