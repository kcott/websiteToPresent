<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class EntryFormSelect extends EntryForm{
		/**
		 * Options to display
		 * @var array of string
		 */
		private $_options;
		/**
		 * Create a select to a form
		 * @param string $name
		 * @param string $text
		 * @param string array $constraints
		 */
		public function __construct(
				$name,
				$text,
				$constraints){
			parent::__construct($name, '', $text, $constraints);
			$this->_options = array();
		}
		/**
		 * Add an option to the select
		 * @param string $key
		 * @param string $value
		 */
		public function addOption($key, $value){
			$this->_options[$key] = $value;
		}
		/**
		 * Display the input with html tags
		 */
		public function generate(){
			$id = 'form_'.$this->getName().'_'.$this->getValue();
			echo '<tr>';
			echo '<td><label for="'.$id.'">'.$this->getText().'</label></td>';
			echo '<td>';
			echo '<select name="'.$this->getName().'" ';
			echo 'id="'.$id.'" ';
			foreach ($this->getConstraints() as $c){
				echo $c.' ';
			}
			echo '>';
			foreach ($this->_options as $key => $value){
				echo '<option value="'.$key.'">'.$value.'</option>';
			}
			echo '</select>';
			echo '</td>';
			echo '</tr>';
		}
		/**
		 * Check whether the entry of the form is valid
		 * @return boolean
		 */
		public function validate(){
			if(array_key_exists("required", $this->getConstraints()))
				if(!isset($_POST[$this->getName()]) || !EntryForm::isIsset($_POST[$this->getName()]))
					return false;
			return true;
		}
	}
?>