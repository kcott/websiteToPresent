<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class EntryFormValidation extends EntryFormInput{
		/**
		 * Create an entry to a form
		 */
		public function __construct(){
			$this->_setType('image');
		}
		/**
		 * Display submit and cancel button with html tags
		 */
		public function generate(){
			echo '<tr><td colspan="2">';
			echo '<input type="image" type="submit"
		 				name="submit"
						height="20px"
						src="../../images/icons/valid.png"
						onclick=""/>
					<input type="image"
		 				name="cancel"
						height="20px"
						src="../../images/icons/cancel.png"
						onclick=""/>';
			echo '</td></tr>';
		}
		/**
		 * Return always true<br/>
		 * Use by recursion
		 * @return boolean
		 */
		public function validate(){
			return true;
		}
	}
?>