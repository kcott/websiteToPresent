<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	abstract class EntryForm extends Webform{
		/**
		 * Defined the name of the entry in the form
		 * @var string
		 */
		private $_name;
		/**
		 * Defined the value of the entry in the form
		 * @var string
		 */
		private $_value;
		/**
		 * Defined the label of the entry in the form
		 * @var string
		 */
		private $_text;
		/**
		 * Defined constraints of the entry in the form
		 * @var array of string
		 */
		private $_constraints;
		/**
		 * Construct an entry form
		 * @param string $name
		 * @param string $value
		 * @param string $text
		 * @param string $constraints
		 */
		public function __construct(
				$name,
				$value,
				$text,
				$constraints){
			$this->_name = $name;
			$this->_value = $value;
			$this->_text = $text;
			$this->_constraints = $constraints;
		}
		/**
		 * Return the name of the entry
		 * @return string
		 */
		public function getName(){
			return $this->_name;
		}
		/**
		 * Set the name of the entry
		 * @param string $name
		 */
		public function setName($name){
			$this->_name = $name;
		}
		/**
		 * Return the value of the entry
		 * @return string
		 */
		public function getValue(){
			return $this->_value;
		}
		/**
		 * Set the value of the entry
		 * @param string $value
		 */
		public function setValue($value){
			$this->_value = $value;
		}
		/**
		 * Return the text of the entry
		 * @return string
		 */
		public function getText(){
			return $this->_text;
		}
		/**
		 * Set the text of the entry
		 * @param string $text
		 */
		public function setText($text){
			$this->_text = $text;
		}
		/**
		 * Return constraints of the entry
		 * @return string array
		 */
		public function getConstraints(){
			return $this->_constraints;
		}
		/**
		 * Set the constraints of the entry
		 * @param string array $constraints
		 */
		public function setConstraints($constraints){
			$this->_constraints = $constraints;
		}
		/**
		 * Check whether url is a url from webpage
		 * @param url $pathname
		 * @return boolean
		 */
		public static function isWebpage($pathname){
			$return = false;
			$ext	= substr($pathname, -4);
			if ($ext == ".php"){
				$return = true;
			}else{
				$ext	= substr($pathname, -5);
				if ($ext == ".html")
					$return = true;
			}
			return $return;
		}
		/**
		 * Check whether a string is isset without spaces
		 * @param string $text
		 * @return boolean
		 */
		public static function isIsset($text){
			$pattern		= "/\s/";
			$replacement	= "";
			$result			= preg_replace($pattern, $replacement, $text);
			if (0<strlen($result))
				return true;
			else
				return false;
		}
	}
?>