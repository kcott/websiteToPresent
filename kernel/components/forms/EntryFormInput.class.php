<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	abstract class EntryFormInput extends EntryForm{
		private $_type;
		/**
		 * Create an entry to a form
		 * @param string $name
		 * @param string $value
		 * @param string $text
		 * @param array with key the constraint and the element the value $constraints
		 */
		public function __construct(
				$type,
				$name,
				$value,
				$text,
				$constraints){
			parent::__construct($name, $value, $text, $constraints);
			$this->_type = $type;
		}
		/**
		 * Return the type of the entry
		 * @return string
		 */
		public function getType(){
			return $this->_type;
		}
		/**
		 * Set the type of the entry
		 * @var string $type
		 */
		protected function _setType($type){
			$this->_type = $type;
		}
		/**
		 * Display the input with html tags
		 */
		public function generate(){
			$id = 'form_'.$this->getName().'_'.$this->getValue();
			echo '<tr>';
			echo '<td><label for="'.$id.'">'.$this->getText().'</label></td>';
			echo '<td>';
			echo '<input name="'.$this->getName().'" ';
			echo 'type="'.$this->getType().'" ';
			echo 'value="'.$this->getValue().'" ';
			echo 'id="'.$id.'" ';
			foreach ($this->getConstraints() as $c){
				echo $c.' ';
			}
			echo '/>';
			echo '</td>';
			echo '</tr>';
		}
		/**
		 * Check whether the entry of the form is valid
		 * @return boolean
		 */
		public function validate(){
			if(array_key_exists("required", $this->getConstraints()))
				if(!isset($_POST[$this->getName()]) || !EntryForm::isIsset($_POST[$this->getName()]))
					return false;
			return true;
		}
	}
?>