<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class EntryFormText extends EntryFormInput{
		/**
		 * Create an entry to a form
		 * @param string $name
		 * @param string $value
		 * @param string $text
		 * @param string array $constraints
		 */
		public function __construct(
				$name,
				$value,
				$text,
				$constraints){
			parent::__construct('text', $name, $value, $text, $constraints);
		}
	}
?>