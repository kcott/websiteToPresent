<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	/**
	 * Define a singleton class to use SSL
	 */
	final class Ssl{
		private function __construct(){}
		private function __clone(){}
		/**
		 * Activate ssl
		 * @param boolean $bool
		 */
		public static function activate($bool){
			if ($bool==true){
				if(!$_SERVER['HTTPS']=='on'){
					header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
				}
			}else{
				if(isset($_SERVER['HTTPS'])){
					if($_SERVER['HTTPS']=='on'){
						header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
					}
				}
			}
		}
	}
?>