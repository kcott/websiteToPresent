<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class Encryption
	{
		/**
		 * Key to crypt and to encrypt
		 * @var string
		 */
		private $_key;
		/**
		 * Construct an encryption
		 * @param string $key
		 */
		public function __construct($key) {
			$this->_key = $key;
		}
		/**
		 * Decrypt a text
		 * @param string $text
		 * @return string
		 */
		public function decrypt($text){
			return $this->decrypt_key($text, $this->_key);
		}
		/**
		 * Decrypt a text
		 * @param string $text
		 * @param string $key
		 * @return string
		 */
		private function decrypt_key($text, $key){
			return trim(mcrypt_decrypt(
					MCRYPT_RIJNDAEL_256,
					$key,
					base64_decode($text),
					MCRYPT_MODE_ECB,
					mcrypt_create_iv(
						mcrypt_get_iv_size(
							MCRYPT_RIJNDAEL_256,
							MCRYPT_MODE_ECB),
						MCRYPT_RAND)));
		}
		/**
		 * Encrypt a text
		 * @param string $text
		 * @return string
		 */
		public function encrypt($text){
			return $this->encrypt_key($text, $this->_key);
		}
		/**
		 * Encrypt a text
		 * @param string $text
		 * @param string $key
		 * @return string
		 */
		private function encrypt_key($text, $key){
			return trim(base64_encode(mcrypt_encrypt(
					MCRYPT_RIJNDAEL_256,
					$key,
					$text,
					MCRYPT_MODE_ECB,
					mcrypt_create_iv(
						mcrypt_get_iv_size(
							MCRYPT_RIJNDAEL_256,
							MCRYPT_MODE_ECB),
						MCRYPT_RAND))));
		}
	}
?>