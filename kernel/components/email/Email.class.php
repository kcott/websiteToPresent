<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class Email
	{
		/**
		 * Address to the sender
		 * @var string
		 */
		private $_from;
		/**
		 * Address to reply a email
		 * @var string
		 */
		private $_replyTo;
		/**
		 * Construct an email
		 * @param string $from
		 * @param string $replyTo
		 */
		public function __construct($from, $replyTo){
			$this->_from = $from;
			$this->_replyTo = $replayTo;
		}
		/**
		 * Get the sender
		 */
		public function getFrom(){
			return $this->_from;
		}
		/**
		 * Set the sender
		 * @param string $from
		 */
		public function setFrom($from){
			$this->_from = $from;
		}
		/**
		 * Get the address to reply a email
		 */
		public function getReplyTo(){
			return $this->_replyTo;
		}
		/**
		 * Set the address to reply a email
		 * @param string $replyTo
		 */
		public function setReplyTo($replyTo){
			$this->_replyTo = $replyTo;
		}
		/**
		 * Send an email<br/>
		 * Return true if all receiver as receive this email
		 * @param string array $to
		 * @param string $subject
		 * @param string $content
		 * @return boolean
		 */
		public function send($to, $subject, $content){
			$headers = "To: ";
			foreach ($to as $aux){
				$headers .= "<".$aux.">\n";
			}
			$headers .= "_from: ".$this->_from."\n";
			$headers .= "Reply-To: ".$this->_replyTo."\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=\"utf-8\"\n";
			foreach ($to as $aux){
				if(mail($aux, $subject, $content, $headers)){
					return true;
				}else{
					return false;
				}
			}
		}
	}
?>