<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class Logfile{
		/**
		 * log file
		 * @var stream
		 */
		private $_handle;
		/**
		 * Create a log file<br/>
		 * Append mode
		 * @param string $filepath
		 */
		public function __construct($filepath){
			$this->_handle	= fopen($filepath, "a");
		}
		/**
		 * Destruct the logfile
		 */
		public function __destruct(){
			fclose($this->_handle);
		}
		/**
		 * Write a message to the log
		 * @param string $message
		 */
		public function write($message){
			$today = getdate();
		
			fwrite($this->_handle,'['	
						.$today['year'].'-'
						.$today['mon'].'-'
						.$today['mday'].']['
						.$today['hours'].':'
						.$today['minutes'].':'
						.$today['seconds'].']');
			fwrite($this->_handle,' '.'['
						.$_SERVER["REMOTE_ADDR"].']');
			fwrite($this->_handle,' '
						.$message);
			fwrite($this->_handle, "\n");
		}
	}
?>