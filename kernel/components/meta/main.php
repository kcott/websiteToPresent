<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	function alert($message){
		echo '<script type="text/javascript">alert("'.$message.'");</script>';
	}
	function redirectionToMainAddress(){
		redirection("http://".$_SERVER["SERVER_NAME"]);
	}
	function redirection($address){
		echo '<meta http-equiv="refresh" content="0;url='.$address.'"/>';
	}
?>