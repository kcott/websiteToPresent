<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	abstract class Database{
		/**
		 * Execute an select statement in the database
		 * @param string $statement
		 */
		abstract public function select($statement);
		/**
		 * Execute an insert statement in the database
		 * @param string $statement
		*/
		abstract public function insert($statement);
		/**
		 * Execute an update statement in the database
		 * @param string $statement
		*/
		abstract public function update($statement);
		/**
		 * Execute an delete statement in the database
		 * @param string $statement
		*/
		abstract public function delete($statement);
	}
?>