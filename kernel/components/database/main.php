<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license propriatery license
	 */
?>
<?php
	require_once __DIR__.'/Database.class.php';
	require_once __DIR__.'/MySqlDatabase.class.php';
	require_once __DIR__.'/PgDatabase.class.php';
?>