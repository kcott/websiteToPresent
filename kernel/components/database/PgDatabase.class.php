<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	final class PgDatabase extends Database
	{
		/**
		 * Contain the database
		 * @var PDO
		 */
		private $_databaseObject;
		/**
		 * Connection with a database postgresql
		 */
		public function __construct()
		{
			// Test if the address is authorized to connect
			$authorized = array(
					'websiteToPresent' => array('dev')
			);
			$domains 			= explode(".", $_SERVER['SERVER_NAME']);
			$domainName 		= $domains[count($_SERVER['SERVER_NAME'])];
			$domainExtension 	= $domains[count($_SERVER['SERVER_NAME'])+1];
			if(!array_key_exists($domainName, $authorized))
				die();
			if(!in_array($domainExtension, $authorized[$domainName], true))
				die();
			
			// Connect to the database
			$pdoOptions[PDO::ATTR_ERRMODE]	= PDO::ERRMODE_EXCEPTION;
			$server							= 'pgsql:host=';
			$host							= 'localhost';
			$database						= 'dbname=websiteToPresent';
			$userDatabase					= 'postgres';
			$passwordDatabase				= 'root';
			$params 						= array(
					PDO::ATTR_PERSISTENT => true
			);
			$this->_databaseObject = new PDO($server.$host.";".$database, $userDatabase, $passwordDatabase, $params);
		}
		/**
		 * Execute an select statement in the database
		 * @param string $statement
		 */
		public function select($statement){
			return $this->_databaseObject->query($statement);
		}
		/**
		 * Execute an insert statement in the database
		 * @param string $statement
		 */
		public function insert($statement){
			return $this->_databaseObject->query($statement);
		}
		/**
		 * Execute an update statement in the database
		 * @param string $statement
		 */
		public function update($statement){
			return $this->_databaseObject->query($statement);
		}
		/**
		 * Execute an delete statement in the database
		 * @param string $statement
		 */
		public function delete($statement){
			return $this->_databaseObject->query($statement);
		}
	}
?>