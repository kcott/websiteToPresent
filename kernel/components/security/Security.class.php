<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class Security{
		/**
		 * Change a code to set vulnerable inputs to xss vulnerabilities
		 * @param string $input
		 * @return string
		 */
		public static function codeHtml($input){
			strip_tags($input);
			$result = htmlentities($input, ENT_QUOTES, "UTF-8");
			return $result;
		}
		/**
		 * Decode what which wad code with code_html function
		 * @param string $input
		 * @return string
		 */
		public static function decodeHtml($input){
			return html_entity_decode($input);
		}
		/**
		 * Change every $_POST to set vulnerable inputs to xss vulnerabilities
		 */
		public static function codeSuperglobalePost(){
			foreach ($_POST as $key => $value){
				$_POST[$key] = Security::codeHtml($value);
			}
		}
		/**
		 * Change every $_GET to set vulnerable inputs to xss vulnerabilities
		 */
		public static function codeSuperglobaleGet(){
			foreach ($_GET as $key => $value){
				$_GET[$key] = Security::codeHtml($value);
			}
		}
	}
?>