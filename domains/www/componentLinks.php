<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	ComponentWebpage::addLinkToForm("http://".$_SERVER["SERVER_NAME"]."/webpage/form");
	ComponentWebpage::addLinkToFormWithId("http://".$_SERVER["SERVER_NAME"]."/webpage/form/");
	ComponentWebpage::addLinkToWebpages("http://".$_SERVER["SERVER_NAME"]."/webpage/webpages");
	
	ComponentUser::addLinkToFormGroup("http://".$_SERVER["SERVER_NAME"]."/user/formGroup");
	ComponentUser::addLinkToFormGroupWithId("http://".$_SERVER["SERVER_NAME"]."/user/formGroup/");
	ComponentUser::addLinkToGroups("http://".$_SERVER["SERVER_NAME"]."/user/groups");
	ComponentUser::addLinkToFormSex("http://".$_SERVER["SERVER_NAME"]."/user/formSex");
	ComponentUser::addLinkToFormSexWithId("http://".$_SERVER["SERVER_NAME"]."/user/formSex/");
	ComponentUser::addLinkToSexs("http://".$_SERVER["SERVER_NAME"]."/user/sexs");
	ComponentUser::addLinkToFormUser("http://".$_SERVER["SERVER_NAME"]."/user/formUser");
	ComponentUser::addLinkToFormUserWithId("http://".$_SERVER["SERVER_NAME"]."/user/formUser/");
	ComponentUser::addLinkToUsers("http://".$_SERVER["SERVER_NAME"]."/user/users");
?>