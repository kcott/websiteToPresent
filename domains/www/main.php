<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<!DOCTYPE html>
<?php
	include_once __DIR__.'/getGlobalVar.php';
	include_once __DIR__.'/componentLinks.php';
	
	$class = $this->getNameClassComponent(URL_COMPONENT);
	if(URL_ARG_1 != ''){
		$object = new $class(array(
				'id' => URL_ARG_1
		));
	}else{
		$object = new $class(array());
	}
	$object->doAction(URL_ACTION);
?>