<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	// Change $_GET to adapt it to this domain
	
	$URL_COMPONENT = "";
	if (!empty($_GET['get1']))
		$URL_COMPONENT = $_GET['get1'];
	define("URL_COMPONENT",$URL_COMPONENT);
	
	$URL_ACTION = "";
	if (!empty($_GET['get2']))
		$URL_ACTION = $_GET['get2'];
	define("URL_ACTION",$URL_ACTION);
	
	$URL_ARG_1 = "";
	if (!empty($_GET['get3']))
		$URL_ARG_1 = $_GET['get3'];
	define("URL_ARG_1",$URL_ARG_1);

	$URL_ARG_2 = "";
	if (!empty($_GET['get4']))
		$URL_ARG_2 = $_GET['get4'];
	define("URL_ARG_2",$URL_ARG_2);

	$URL_ARGS = "";
	if (!empty($_GET['get5']))
		$URL_ARGS = $_GET['get5'];
	define("URL_ARGS",$URL_ARGS);
?>