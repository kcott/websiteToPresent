<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<!DOCTYPE html>
<html5>
	<body>
		<p><?php echo $_SERVER["SERVER_NAME"]?></p>
		<?php
			// Test if the address is authorized to connect
			$authorized = array(
					'v2015_03_modular' => array('dev')
			);
			$domains 			= explode(".", $_SERVER['SERVER_NAME']);
			$domainName 		= $domains[count($_SERVER['SERVER_NAME'])];
			$domainExtension 	= $domains[count($_SERVER['SERVER_NAME'])+1];
			if(!array_key_exists($domainName, $authorized))
				die();
			if(!in_array($domainExtension, $authorized[$domainName], true))
				die();
		?>
		<p>Test</p>
		<p><?php
			// Create form
			$form = new Form("Formulary test");
			
			// Create a select
			$efSelect = new EntryFormSelect("country", "Select your country", array('required'));
			$efSelect->addOption("fr", "France");
			$efSelect->addOption("us", "United State");
			$efSelect->addOption("uk", "United Kingtown");
			$form->add($efSelect);
			
			// Create a radio
			$efRadio = new EntryFormRadio("sex", "Select your sex", array('required'));
			$efRadio->addValue("m", "men", array('required'));
			$efRadio->addValue("w", "women", array());
			$form->add($efRadio);
			
			// Create a checkbox
			$efCheckbox = new EntryFormCheckbox('hair_color', 'Select your hair color', array());
			$efCheckbox->addValue('black', 'black', array('required'));
			$efCheckbox->addValue('grey', 'grey', array());
			$efCheckbox->addValue('blond', 'blond', array());
			$efCheckbox->addValue('red', 'red', array());
			$efCheckbox->addValue('white', 'white', array());
			$form->add($efCheckbox);
			
			// Generate the form
			$form->generate();
		?></p>
		<?php
			echo '<p>';
			echo 'count $_SERVER[\'SERVER_NAME\']:'.count(explode(".", $_SERVER['SERVER_NAME'])).'<br/>';
			echo '$_SERVER[\'SERVER_NAME\']:'.$_SERVER['SERVER_NAME'];
			echo '</p>';
		?>
	</body>
</html>