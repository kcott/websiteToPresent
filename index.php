<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	// Desativate reading of session id in URL
	ini_set('session.use_trans_sid', "0");
	// Authorize cookies
	ini_set('session.use_cookies', "1");
	// Ban add session id in html code
	ini_set("url_rewriter.tags","");
	
	session_start();
	
	// Initialize the kernel
	require_once __DIR__.'/kernel/Application.class.php';
	$application = Application::getInstance();
	$application->setDatabase(new PgDatabase());
	
	// Secure the application
	Security::codeSuperglobalePost();
	Security::codeSuperglobaleGet();
	
	// Get the domain
	$sizeAddress = count(explode(".", $_SERVER['SERVER_NAME']));
	if($sizeAddress>=3){
		$domain = explode(".", $_SERVER['SERVER_NAME'])[$sizeAddress - 3];
		if(file_exists('domains/'.$domain))
			$application->getDomain($domain);
		else{
			$domains = explode(".", $_SERVER['SERVER_NAME']);
			$address = 'www.'.$domains[1].'.'.$domains[2];
			header('Location:http://'.$address);
		}
	}else{
		header('Location:http://'.$address);
	}
?>