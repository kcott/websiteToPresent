/**
 * @author Kevin Cottin
 * @copyright 2015 Kevin Cottin
 * @license proprietary license
 */
function is_color_hexadecimal(value,tag){
	var value2 = value.replace(/\s/g,"");
	var result = false;
	
	var pattern = "^[0-9a-fA-F]{6}$";
	var reg = new RegExp(pattern);
	var result = reg.test(value2);
	
	return result;
}
function is_boolean(value){
	var pattern = "^(0|1){1,}$";
	var reg = new RegExp(pattern);
	var result = reg.test(login);
	
	if (result == true)
		return true;
	else
		return false;
}
function is_date(value,idElement){
	var pattern = "^[0-9]{4}-[0-9]{2}-[0-9]{2}$";
	var reg = new RegExp(pattern);
	var result = reg.test(value);
	
	if (result == true){
		var old		= 150;
		var year	= value.substr(0,4);
		var month	= value.substr(5,2);
		var day		= value.substr(8,2);
		
		var date		= new Date(year,month-1,day);
		var currentDate = new Date();
		
		if (date<currentDate){
			if ((currentDate.getFullYear()-old)<year){
				if ((0<month)&&(month<=12)){
					if !((0<day)&&(day<=31))
						result = false; 
				} else result = false; 
			} else result = false; 
		} else result = false; 
	}
	return result;
}
function is_email(value,idElement){
	var value2 = value.replace(/\s/g,"");
	var result = false;
	
	if ((0<value2.length)&&(value2.length<=320))
		result = true;
	else
		result = false;
	if (result == true){
		var pattern = "^[a-zA-Z0-9-._]{1,}@[a-zA-Z0-9-.]{1,}$";
		var reg = new RegExp(pattern);
		result = reg.test(value);
	}
	return result;
}
function is_number(value){
	var pattern = "^[0-9]{1,}$";
	var reg = new RegExp(pattern);
	var result = reg.test(login);

	return result;
}
function is_phone_number(value,idElement){
	var result = true;
	if (isset(value,idElement)==true){
		var pattern = "^[0-9]{1,}$";
		var reg = new RegExp(pattern);
		result = reg.test(value);
	}
	return result;
}
function is_sex(value,idElement){
	var pattern = "^(F|M){1}$";
	var reg = new RegExp(pattern);
	var result = reg.test(value);
	
	return result;
}
function isset(value,idElement){
	var value2 = value.replace(/\s/g,"");
	var result = false;
	
	if (value2.length!=0)
		result = true;
	return result;
}
function is_webpage(idElement){
	var value = document.getElementById(idElement).value;
	var pattern = ".php$";
	var reg = new RegExp(pattern);
	result = reg.test(value);
	if (result==false){
		var pattern = ".html$";
		var reg = new RegExp(pattern);
		result = reg.test(value);
	}
	if (result==false){
		alert('Format non accepté\n\nFormat accepté :\n.php .html');
		document.getElementById(idElement).value = "";
	}
	return result;
}
function is_zip(idElement){
	var value = document.getElementById(idElement).value;
	var pattern = ".zip$";
	var reg = new RegExp(pattern);
	result = reg.test(value);
	if (result==false){
		alert('Format non accepté\n\nFormat accepté :\n.zip');
		document.getElementById(idElement).value = "";
	}
	return result;
}