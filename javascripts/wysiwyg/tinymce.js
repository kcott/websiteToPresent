tinyMCE.init({
	mode : "exact",
	elements : "n_text",
	theme : "advanced",
	skin : "default",
	language : "fr",
	plugins : "table,advhr,media,advlink,advimage,paste",
	file_browser_callback : "filebrowser",
	
	theme_advanced_toolbar_location : "top",
	theme_advanced_buttons1 :
		  "bold,italic,underline,strikethrough,separator,"
		+ "justifyleft,justifycenter,justifyright,justifyfull,separator,"
		+ "fontselect,fontsizeselect,forecolor,separator,"
		+ "formatselect,separator,"
		+ "bullist,numlist,outdent,indent,advhr,separator,"
		+ "spellchecker,code",
	theme_advanced_buttons2 :
		  "pastetext,pasteword,selectall,separator,"
		+ "link,unlink,anchor,image,media,tablecontrols,separator"
		+ "sub,sup,charmap,separator,"
		+ "undo,redo,cleanup",
	theme_advanced_buttons3_add :
		  "fullpage, tablecontrols",
	theme_advanced_fonts :
		  "Andale Mono=andale mono,times;"
        + "Arial=arial,helvetica,sans-serif;"
        + "Arial Black=arial black,avant garde;"
        + "Book Antiqua=book antiqua,palatino;"
        + "Calibri=calibri;"
        + "Comic Sans MS=comic sans ms,sans-serif;"
        + "Courier New=courier new,courier;"
        + "Georgia=georgia,palatino;"
        + "Helvetica=helvetica;"
        + "Impact=impact,chicago;"
        + "Symbol=symbol;"
        + "Tahoma=tahoma,arial,helvetica,sans-serif;"
        + "Terminal=terminal,monaco;"
        + "Times New Roman=times new roman,times;"
        + "Trebuchet MS=trebuchet ms,geneva;"
        + "Verdana=verdana,geneva;"
        + "Webdings=webdings;"
        + "Wingdings=wingdings,zapf dingbats"
});

function filebrowser(field_name, url, type, win)
{
	fileBrowserURL = "../pdw_file_browser/index.php?editor=tinymce&filter=" + type;
	
	tinyMCE.activeEditor.windowManager.open(
		{
			title: "PDW File Browser",
				url: fileBrowserURL,
				width: 950,
				height: 650,
				inline: 0,
				maximizable: 1,
				close_previous: 0
		},
		{
			window : win,
			input : field_name
		}
	);    
}