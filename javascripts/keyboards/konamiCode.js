/**
 * @author Kevin Cottin
 * @copyright 2015 Kevin Cottin
 * @license proprietary license
 */
if (window.addEventListener)
{
	var kkeys = [];
	var konami =
		KEY_UP + "," +
		KEY_UP + "," +
		KEY_DOWN + "," +
		KEY_DOWN + "," +
		KEY_LEFT + "," +
		KEY_RIGHT + "," +
		KEY_LEFT + "," +
		KEY_RIGHT + "," +
		KEY_B + "," +
		KEY_A;
	window.addEventListener("keydown", function(e)
	{
		kkeys.push(e.keyCode);
		if (kkeys.toString().indexOf(konami)>= 0)
		{
			konamiCode();
		}
	},true)
}

function konamiCode()
{
	//window.location = "http://www.google.fr";
}