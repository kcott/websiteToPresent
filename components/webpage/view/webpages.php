<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$webpage = new Webpage();
?>
<table>
	<tr>
		<th><?php echo $lang_id; ?></th>
		<th><?php echo $lang_alias; ?></th>
		<th><?php echo $lang_datecreate; ?></th>
		<th><?php echo $lang_dateupdate; ?></th>
		<th><?php echo $lang_title; ?></th>
	</tr>
<?php
	$statement = $webpage->statementWebpages();
	foreach(Application::getInstance()->getDatabase()->select($statement) as $result){
		echo '<tr>';
		echo '<td>'.$result['id'].'</td>';
		echo '<td>'.$result['alias'].'</td>';
		echo '<td>'.$result['date_create'].'</td>';
		echo '<td>'.$result['date_update'].'</td>';
		echo '<td>'.$result['title'].'</td>';
		echo '</tr>';
	}
?>
</table>
