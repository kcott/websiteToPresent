<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$webpage = new Webpage();
	
	$aliasWebpage;
	if(array_key_exists('id', $this->getParams()))
		$aliasWebpage = $this->getParams()['id'];
	else
		$aliasWebpage = '';
	
	if(isset($_POST['submit_x'])){
		$webpage->setAlias($_POST['alias']);
		$webpage->setTitle($_POST['title']);
		$webpage->setMetaDescription($_POST['meta_description']);
		$webpage->setMetaKeywords($_POST['meta_keywords']);
	}elseif($aliasWebpage != ''){
		$webpage->getWebpageByAlias($aliasWebpage);
	}

	$alias = new EntryFormText('alias', $webpage->getAlias(), $lang_alias, array('readonly'=>'readonly'));
	$title = new EntryFormText('title', $webpage->getTitle(), $lang_title, array('required'=>'required'));
	$metaDescription = new EntryFormTextarea('meta_description', $webpage->getMetaDescription(), $lang_description, array());
	$metaKeywords = new EntryFormTextarea('meta_keywords', $webpage->getMetaKeywords(), $lang_keywords, array());
	
	$form = new Form($lang_metadatas);
	$form->add($alias);
	$form->add($title);
	$form->add($metaDescription);
	$form->add($metaKeywords);
	$form->add(new EntryFormValidation());
	
	if($form->validate()){
		if($aliasWebpage == '')
			Application::getInstance()->getDatabase()->insert($webpage->statementInsert());
		else
			Application::getInstance()->getDatabase()->update($webpage->statementUpdate());
		redirection(ComponentWebpage::getLinkToWebpages());
	}elseif($form->isCanceled()){
		redirection(ComponentWebpage::getLinkToWebpages());
	}
?>
<form method="post" action="" onsubmit="" enctype="multipart/form-data">
	<?php
		$form->generate();
	?>
</form>