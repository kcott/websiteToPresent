<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$lang_alias = 'Alias';
	$lang_cancel = 'Cancel';
	$lang_datecreate = 'Create date';
	$lang_dateupdate = 'Update date';
	$lang_description = 'Description';
	$lang_id = 'Id';
	$lang_keywords = 'Key words';
	$lang_metadatas = 'Metadata';
	$lang_title = 'Title';
	$lang_valid = 'Valid';
?>