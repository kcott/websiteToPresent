<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$lang_alias = 'Alias';
	$lang_cancel = 'Annuler';
	$lang_datecreate = 'Date de cr&eacute;ation';
	$lang_dateupdate = 'Date de modification';
	$lang_description = 'Description';
	$lang_id = 'Id';
	$lang_keywords = 'Mots clefs';
	$lang_metadatas = 'M&eacute;tadonn&eacute;es';
	$lang_title = 'Titre';
	$lang_valid = 'Valider';
?>