<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class Webpage{
		private static $_NAME_TABLE = "webpages"; 
		
		private $_id;
		private $_alias;
		private $_dateCreate;
		private $_dateUpdate;
		private $_metaDescription;
		private $_metaKeywords;
		private $_title;
		
		public function __construct(){}
		public function set(
				$id,
				$alias,
				$dateCreate,
				$dateUpdate,
				$metaDescription,
				$metaKeywords,
				$title){
			$this->_id = $id;
			$this->_alias = $alias;
			$this->_dateCreate = $dateCreate;
			$this->_dateUpdate = $dateUpdate;
			$this->_metaDescription = $metaDescription;
			$this->_metaKeywords = $metaKeywords;
			$this->_title = $title;
		}
		public function getAlias(){
			return $this->_alias;
		}
		public function setAlias($alias){
			$this->_alias = $alias;
		}
		public function getId(){
			return $this->_id;
		}
		public function setId($id){
			$this->_id = $id;
		}
		public function getDateCreate(){
			return $this->_dateCreate;
		}
		public function setDateCreate($dateCreate){
			$this->_dateCreate = $dateCreate;
		}
		public function getDateUpdate(){
			return $this->_dateUpdate;
		}
		public function setDateUpdate($dateUpdate){
			$this->_dateUpdate = $dateUpdate;
		}
		public function getMetaDescription(){
			return $this->_metaDescription;
		}
		public function setMetaDescription($metaDescription){
			$this->_metaDescription = $metaDescription;
		}
		public function getMetaKeywords(){
			return $this->_metaKeywords;
		}
		public function setMetaKeywords($metaKeywords){
			$this->_metaKeywords = $metaKeywords;
		}
		public function getTitle(){
			return $this->_title;
		}
		public function setTitle($title){
			$this->_title = $title;
		}
		public function __destruct(){}
		
		public function getWebpageByAlias($alias){
			$statement = $this->statementWebpageByAlias($alias);
			foreach(Application::getInstance()->getDatabase()->select($statement) as $result){
				$this->_id = $result['id'];
				$this->_alias = $result['alias'];
				$this->_dateCreate = $result['date_create'];
				$this->_dateUpdate = $result['date_update'];
				$this->_metaDescription = $result['meta_description'];
				$this->_metaKeywords = $result['meta_keywords'];
				$this->_title = $result['title'];
			}
		}
		public function statementWebpageByAlias($alias){
			return "SELECT
						id					AS id,
						alias				AS alias,
						date_create			AS date_create,
						date_update 		AS date_update,
						meta_description 	AS meta_description,
						meta_keywords 		AS meta_keywords,
						title 				AS title
					FROM ".self::$_NAME_TABLE." w
					WHERE alias = '".$alias."'";
		}
		public function statementWebpages(){
			return "SELECT
						id					AS id,
						alias				AS alias,
						date_create			AS date_create,
						date_update 		AS date_update,
						meta_description 	AS meta_description,
						meta_keywords 		AS meta_keywords,
						title 				AS title
					FROM ".self::$_NAME_TABLE." w
					ORDER BY alias";
		}
		public function statementWebpagesByOrders($orders){
			if(!is_isset($orders))
				return;
			return "SELECT
						id					AS id,
						alias				AS alias,
						date_create			AS date_create,
						date_update 		AS date_update,
						meta_description 	AS meta_description,
						meta_keywords 		AS meta_keywords,
						title 				AS title
					FROM ".self::$_NAME_TABLE." w
					ORDER BY ".$orders;
		}
		public function statementInsert(){
			return "insert into ".self::$_NAME_TABLE." values(
						DEFAULT,
						NULL,
						CURRENT_TIMESTAMP,
						CURRENT_TIMESTAMP,
						'".$this->_metaDescription."',
						'".$this->_metaKeywords."',
						'".$this->_title."')";
		}
		public function statementUpdate(){
			return "update ".self::$_NAME_TABLE." set
						title='".$this->_title."',
						date_update=CURRENT_TIMESTAMP,
						meta_description='".$this->_metaDescription."',
						meta_keywords='".$this->_metaKeywords."'
					where alias='".$this->_alias."'";
		}
		public function statementDelete(){
			return "delete from ".self::$_NAME_TABLE."
							where alias='".$this->_alias."'";
		}
	}
?>