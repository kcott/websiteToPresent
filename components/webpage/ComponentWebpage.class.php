<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	require_once __DIR__.'/model/Webpage.class.php';
		
	class ComponentWebpage extends Component{
		public function __construct($params){
			parent::__construct($params);
			$this->addAction(
					'form',
					new ComponentAction('form', ActionTypeComponent::FORM)
			);
			$this->addAction(
					'webpages',
					new ComponentAction('webpages', ActionTypeComponent::DISPLAY)
			);
		}
		public function doAction($action){
			include_once __DIR__.'/lang.php';
			switch ($action){
				case 'form':
					include __DIR__.'/view/form.php';
					break;
				case 'webpages':
					include __DIR__.'/view/webpages.php';
					break;
				default:
					break;
			}
		}
		static public function addLinkToWebpages($link){
			self::addLink('webpages', $link);
		}
		static public function getLinkToWebpages(){
			return self::getLink('webpages');
		}
		static public function addLinkToForm($link){
			self::addLink('form', $link);
		}
		static public function getLinkToForm(){
			return self::getLink('form');
		}
		static public function addLinkToFormWithId($link){
			self::addLink('formId', $link);
		}
		static public function getLinkToFormWithId($id){
			return self::getLink('formId').'/'.$id;
		}
	}
?>