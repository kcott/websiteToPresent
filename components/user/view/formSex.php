<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$sex = new UserSex();
	
	$idSex;
	if(array_key_exists('id', $this->getParams()))
		$idSex = $this->getParams()['id'];
	else
		$idSex = '';
	
	if(isset($_POST['submit_x'])){
		$sex->setId($_POST['id']);
		$sex->setName($_POST['name']);
	}elseif($idSex != ''){
		$sex->getSex($idSex);
	}

	$id = new EntryFormText('id', $sex->getId(), $lang_id, array('readonly'=>'readonly'));
	$name = new EntryFormText('name', $sex->getName(), $lang_name, array('required'=>'required'));
	
	$form = new Form($lang_fielsetUserSex);
	$form->add($id);
	$form->add($name);
	$form->add(new EntryFormValidation());
	
	if($form->validate()){
		if($idSex == '')
			Application::getInstance()->getDatabase()->insert($sex->statementInsert());
		else
			Application::getInstance()->getDatabase()->update($sex->statementUpdate());
		redirection(ComponentUser::getLinkToSexs());
	}elseif($form->isCanceled()){
		redirection(ComponentUser::getLinkToSexs());
	}
?>
<form method="post" action="" onsubmit="" enctype="multipart/form-data">
	<?php
		$form->generate();
	?>
</form>