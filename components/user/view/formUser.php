<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$user = new User();
	
	$idUser;
	if(array_key_exists('id', $this->getParams()))
		$idUser = $this->getParams()['id'];
	else
		$idUser = '';
	
	if(isset($_POST['submit_x'])){
		$user->setId($_POST['id']);
		$user->setLogin($_POST['login']);
		$user->setPassword($_POST['password']);
		$user->setFirstName($_POST['first_name']);
		$user->setName($_POST['name']);
		$user->setEmail($_POST['email']);
		$user->setBirthDate($_POST['birth_date']);
		$user->setSex($_POST['sex']);
		$user->setGroup($_POST['group']);
	}elseif($idUser != ''){
		$user->getUser($idUser);
	}
	
	$id = new EntryFormText('id', $user->getId(), $lang_id, array('readonly'=>'readonly'));
	$login = new EntryFormText('login', $user->getId(), $lang_login, array('required'=>'required'));
	$password = new EntryFormPassword('password', $user->getId(), $lang_password, array('required'=>'required'));
	$firstName = new EntryFormText('first_name', $user->getId(), $lang_firstName, array('required'=>'required'));
	$name = new EntryFormText('name', $user->getName(), $lang_name, array('required'=>'required'));
	$email = new EntryFormEmail('email', $user->getId(), $lang_email, array('required'=>'required'));
	$birthDate = new EntryFormDate('birth_date', $user->getId(), $lang_birthDate, array('required'=>'required'));
	$sex = new EntryFormSelect('sex', $lang_fielsetUserSex, array('required'=>'required'));
	foreach (Application::getInstance()->getDatabase()->select((new UserSex())->statementSelect()) as $s){
		$sex->addOption($s['id'], $s['name']);
	}
	$group = new EntryFormSelect('group', $lang_fielsetUserGroup, array('required'=>'required'));
	foreach (Application::getInstance()->getDatabase()->select((new UserGroup())->statementSelect()) as $g){
		$group->addOption($g['id'], $g['name']);
	}
	
	$form = new Form($lang_fielsetUser);
	$form->add($id);
	$form->add($login);
	$form->add($password);
	$form->add($firstName);
	$form->add($name);
	$form->add($email);
	$form->add($birthDate);
	$form->add($sex);
	$form->add($group);
	$form->add(new EntryFormValidation());
	
	if($form->validate()){
		if($idUser == '')
			Application::getInstance()->getDatabase()->insert($user->statementInsert());
		else
			Application::getInstance()->getDatabase()->update($user->statementUpdate());
		redirection(ComponentUser::getLinkToUsers());
	}elseif($form->isCanceled()){
		redirection(ComponentUser::getLinkToUsers());
	}
?>
<form method="post" action="" onsubmit="" enctype="multipart/form-data">
	<?php
		$form->generate();
	?>
</form>