<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$userSex = new UserSex();
?>
<table>
	<tr>
		<th><?php echo $lang_id; ?></th>
		<th><?php echo $lang_name; ?></th>
	</tr>
<?php
	$statement = $userSex->statementSelect();
	foreach(Application::getInstance()->getDatabase()->select($statement) as $result){
		echo '<tr>';
		echo '<td>'.$result['id'].'</td>';
		echo '<td>'.$result['name'].'</td>';
		echo '</tr>';
	}
?>
</table>
