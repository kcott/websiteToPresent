<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$user = new User();
?>
<table>
	<tr>
		<th><?php echo $lang_id; ?></th>
		<th><?php echo $lang_login; ?></th>
		<th><?php echo $lang_password; ?></th>
		<th><?php echo $lang_firstName; ?></th>
		<th><?php echo $lang_name; ?></th>
		<th><?php echo $lang_email; ?></th>
		<th><?php echo $lang_birthDate; ?></th>
		<th><?php echo $lang_sex; ?></th>
		<th><?php echo $lang_group; ?></th>
	</tr>
<?php
	$statement = $user->statementSelect();
	foreach(Application::getInstance()->getDatabase()->select($statement) as $result){
		echo '<tr>';
		echo '<td>'.$result['id'].'</td>';
		echo '<td>'.$result['login'].'</td>';
		echo '<td>'.$result['password'].'</td>';
		echo '<td>'.$result['first_name'].'</td>';
		echo '<td>'.$result['name'].'</td>';
		echo '<td>'.$result['email'].'</td>';
		echo '<td>'.$result['birth_date'].'</td>';
		echo '<td>'.$result['sex'].'</td>';
		echo '<td>'.$result['group'].'</td>';
		echo '</tr>';
	}
?>
</table>
