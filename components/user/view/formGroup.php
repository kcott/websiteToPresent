<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$group = new UserGroup();
	
	$idGroup;
	if(array_key_exists('id', $this->getParams()))
		$idGroup = $this->getParams()['id'];
	else
		$idGroup = '';
	
	if(isset($_POST['submit_x'])){
		$group->setId($_POST['id']);
		$group->setName($_POST['name']);
	}elseif($idGroup != ''){
		$group->getGroup($idGroup);
	}

	$id = new EntryFormText('id', $group->getId(), $lang_id, array('readonly'=>'readonly'));
	$name = new EntryFormText('name', $group->getName(), $lang_name, array('required'=>'required'));
	
	$form = new Form($lang_fielsetUserGroup);
	$form->add($id);
	$form->add($name);
	$form->add(new EntryFormValidation());
	
	if($form->validate()){
		if($idGroup == '')
			Application::getInstance()->getDatabase()->insert($group->statementInsert());
		else
			Application::getInstance()->getDatabase()->update($group->statementUpdate());
		redirection(ComponentUser::getLinkToGroups());
	}elseif($form->isCanceled()){
		redirection(ComponentUser::getLinkToGroups());
	}
?>
<form method="post" action="" onsubmit="" enctype="multipart/form-data">
	<?php
		$form->generate();
	?>
</form>