<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class User{
		private static $_NAME_TABLE = "users";
		
		private $_id;
		private $_login;
		private $_password;
		private $_firstName;
		private $_name;
		private $_email;
		private $_birthDate;
		private $_sex;
		private $_group;
		
		public function getId(){
			return $this->_id;
		}
		public function setId($id){
			$this->_id = $id;
		}
		public function getLogin(){
			return $this->_login;
		}
		public function setLogin($login){
			$this->_login = $login;
		}
		public function getPassword(){
			return $this->_password;
		}
		public function setPassword($password){
			$this->_password = $password;
		}
		public function getFirstName(){
			return $this->_firstName;
		}
		public function setFirstName($firstName){
			$this->_firstName = $firstName;
		}
		public function getName(){
			return $this->_name;
		}
		public function setName($name){
			$this->_name = $name;
		}
		public function getEmail(){
			return $this->_email;
		}
		public function setEmail($email){
			$this->_email = $email;
		}
		public function getBirthDate(){
			return $this->_birthDate;
		}
		public function setBirthDate($birthDate){
			$this->_birthDate = $birthDate;
		}
		public function getSex(){
			return $this->_sex;
		}
		public function setSex($sex){
			$this->_sex = $sex;
		}
		public function getGroup(){
			return $this->_userGroup;
		}
		public function setGroup($group){
			$this->_group = $group;
		}

		public function getUser($id){
			$statement = $this->statementUserById($id);
			foreach(Application::getInstance()->getDatabase()->select($statement) as $result){
				$this->_id = $result['id'];
				$this->_login = $result['login'];
				$this->_password = $result['password'];
				$this->_firstName = $result['first_name'];
				$this->_name = $result['name'];
				$this->_email = $result['email'];
				$this->_birthDate = $result['birth_date'];
				$this->_sex = $result['sex'];
				$this->_userGroup = $result['group'];
			}
		}
		public function statementUserById($id){
			return "SELECT
						id					AS id,
						login				AS login,
						password			AS password,
						first_name			AS first_name,
						name				AS name,
						email				AS email,
						birth_date			AS birth_date,
						fk_user_sex			AS sex,
						fk_user_group		AS group
					FROM ".self::$_NAME_TABLE."
					WHERE id = '".$id."'";
		}
		public function statementSelect(){
			return "SELECT
						id					AS id,
						login				AS login,
						password			AS password,
						first_name			AS first_name,
						name				AS name,
						email				AS email,
						birth_date			AS birth_date,
						fk_user_sex			AS sex,
						fk_user_group		AS group
					FROM ".self::$_NAME_TABLE;
		}
		public function statementInsert(){
			return "insert into ".self::$_NAME_TABLE." values(
						DEFAULT,
						'".$this->_login."',
						'".$this->_password."',
						'".$this->_firstName."',
						'".$this->_name."',
						'".$this->_email."',
						'".$this->_birthDate."',
						NULL,
						NULL,
						NULL,
						NULL,
						'".$this->_sex."',
						'".$this->_group."')";
		}
		public function statementUpdate(){
			return "update ".self::$_NAME_TABLE." set
						login='".$this->_login."',
						password='".$this->_password."',
						first_name='".$this->_firstName."',
						name='".$this->_name."',
						email='".$this->_email."',
						birth_date='".$this->_birthDate."',
						fk_user_sex='".$this->_sex."',
						fk_user_group='".$this->_group."'
					where id='".$this->_id."'";
		}
		public function statementDelete(){
			return "delete from ".self::$_NAME_TABLE."
							where id='".$this->_id."'";
		}
	}
?>