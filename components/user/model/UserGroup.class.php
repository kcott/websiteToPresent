<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	class UserGroup{
		private static $_NAME_TABLE = "user_groups";
		private $_id;
		private $_name;
		public function getId(){
			return $this->_id;
		}
		public function setId($id){
			$this->_id = $id;
		}
		public function getName(){
			return $this->_name;
		}
		public function setName($name){
			$this->_name = $name;
		}
		public function getGroup($id){
			$statement = $this->statementGroupById($id);
			foreach(Application::getInstance()->getDatabase()->select($statement) as $result){
				$this->_id = $result['id'];
				$this->_name = $result['name'];
			}
		}
		public function statementGroupById($id){
			return "SELECT
						id					AS id,
						name				AS name
					FROM ".self::$_NAME_TABLE."
					WHERE id = '".$id."'";
		}
		public function statementSelect(){
			return "select
						id	 AS id,
						name AS name
					from ".self::$_NAME_TABLE."
					order by name";
		}
		public function statementInsert(){
			return "insert into ".self::$_NAME_TABLE." values(
						DEFAULT,
						'".$this->_name."')";
		}
		public function statementUpdate(){
			return "update ".self::$_NAME_TABLE." set
						name='".$this->_name."'
					where id='".$this->_id."'";
		}
		public function statementDelete(){
			return "delete from ".self::$_NAME_TABLE."
							where id='".$this->_id."'";
		}
	}
?>