<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	/*
	 * Get the lang to this component
	*/
	if(is_file(__DIR__.'/lang/_'.Application::getInstance()->getLang().'.php'))
		require_once __DIR__.'/lang/_'.Application::getInstance()->getLang().'.php';
	else
		require_once __DIR__.'/lang/_en.php';
?>