<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$lang_fielsetUserGroup = 'User group';
	$lang_fielsetUserSex = 'Sex of one user';
	$lang_fielsetUser = 'User';
	$lang_id = 'Id';
	$lang_login = 'Login';
	$lang_password = 'Password';
	$lang_firstName = 'First name';
	$lang_name = 'Name';
	$lang_email = 'Email';
	$lang_birthDate = 'Birth date';
	
	$lang_group = 'User group';
	$lang_sex = 'Sex of one user';
	$lang_user = 'User';
?>