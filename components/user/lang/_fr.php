<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	$lang_fielsetUserGroup = 'Groupe d\'utilisateur';
	$lang_fielsetUserSex = 'Sexe d\'un utilisateur';
	$lang_fielsetUser = 'Utilisateur';
	$lang_id = 'Id';
	$lang_login = 'Pseudo';
	$lang_password = 'Mot de passe';
	$lang_firstName = 'Pr&eacute;nom';
	$lang_name = 'Nom';
	$lang_email = 'Email';
	$lang_birthDate = 'Date de naissance';

	$lang_group = 'Groupe d\'utilisateur';
	$lang_sex = 'Sexe d\'un utilisateur';
	$lang_user = 'Utilisateur';
?>