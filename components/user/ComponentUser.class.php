<?php
	/**
	 * @author Kevin Cottin
	 * @copyright 2015 Kevin Cottin
	 * @license proprietary license
	 */
?>
<?php
	require_once __DIR__.'/model/UserGroup.class.php';
	require_once __DIR__.'/model/UserSex.class.php';
	require_once __DIR__.'/model/User.class.php';
	
	class ComponentUser extends Component{
		public function __construct($params){
			parent::__construct($params);
			$this->addAction(
					'groups',
					new ComponentAction('groups', ActionTypeComponent::DISPLAY)
			);
			$this->addAction(
					'formGroup',
					new ComponentAction('formGroup', ActionTypeComponent::FORM)
			);
			$this->addAction(
					'sexs',
					new ComponentAction('sexs', ActionTypeComponent::DISPLAY)
			);
			$this->addAction(
					'formSex',
					new ComponentAction('formSex', ActionTypeComponent::FORM)
			);
			$this->addAction(
					'users',
					new ComponentAction('users', ActionTypeComponent::DISPLAY)
			);
			$this->addAction(
					'formUSer',
					new ComponentAction('formUSer', ActionTypeComponent::FORM)
			);
		}
		public function doAction($action){
			include_once __DIR__.'/lang.php';
			switch ($action){
				case 'groups':
					include __DIR__.'/view/groups.php';
					break;
				case 'formGroup':
					include __DIR__.'/view/formGroup.php';
					break;
				case 'sexs':
					include __DIR__.'/view/sexs.php';
					break;
				case 'formSex':
					include __DIR__.'/view/formSex.php';
					break;
				case 'users':
					include __DIR__.'/view/users.php';
					break;
				case 'formUser':
					include __DIR__.'/view/formUser.php';
					break;
				default:
					break;
			}
		}
		static public function addLinkToGroups($link){
			self::addLink('groups', $link);
		}
		static public function getLinkToGroups(){
			return self::getLink('groups');
		}
		static public function addLinkToFormGroup($link){
			self::addLink('formGroup', $link);
		}
		static public function getLinkToFormGroup(){
			return self::getLink('formGroup');
		}
		static public function addLinkToFormGroupWithId($link){
			self::addLink('formGroupId', $link);
		}
		static public function getLinkToFormGroupWithId($id){
			return self::getLink('formGroupId').'/'.$id;
		}
		
		static public function addLinkToSexs($link){
			self::addLink('sexs', $link);
		}
		static public function getLinkToSexs(){
			return self::getLink('sexs');
		}
		static public function addLinkToFormSex($link){
			self::addLink('formSex', $link);
		}
		static public function getLinkToFormSex(){
			return self::getLink('formSex');
		}
		static public function addLinkToFormSexWithId($link){
			self::addLink('formSexId', $link);
		}
		static public function getLinkToFormSexWithId($id){
			return self::getLink('formSexId').'/'.$id;
		}

		static public function addLinkToUsers($link){
			self::addLink('users', $link);
		}
		static public function getLinkToUsers(){
			return self::getLink('users');
		}
		static public function addLinkToFormUser($link){
			self::addLink('formUser', $link);
		}
		static public function getLinkToFormUser(){
			return self::getLink('formUser');
		}
		static public function addLinkToFormUserWithId($link){
			self::addLink('formUserId', $link);
		}
		static public function getLinkToFormUserWithId($id){
			return self::getLink('formUserId').'/'.$id;
		}
	}
?>